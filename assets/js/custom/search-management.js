$("document").ready(function () {

    $(".filter-form").on("submit", function (e) {
        e.preventDefault();

        $(this).request('SearchHandler::onFilter', {

            'update': {
                'products/products-results': '#result-container'
            }
        }).then(function () {
            //alert("completado");
        });
    });

    $(".search-form").on("submit", function (e) {
        e.preventDefault();

        $(this).request('SearchHandler::onSearch',{
            'update': {
                'products/products-results': '#result-container',
                'filters' : '#page-filter'
            }
        });
    });

    $(".filter-control").on("change", function () {
        var searchForm = $(".search-form").serializeArray();
        var data = convertToObject(searchForm);
        $(this).request('SearchHandler::onFilter',{
            data: data,
            'update': {
                'products/products-results': '#result-container'
            }
        });
    });

    
    
    const datepickerGroup = $("#datepickerGroup");
    const portSelector = $("#port-selector");
    const destinationSelector = $("#destination-selector");

    $("#buy").on("click", function() {
        datepickerGroup.addClass("d-none");
        portSelector.removeClass("d-none");
        destinationSelector.addClass("d-none");
    });
    $("#rent").on("click", function() {
        datepickerGroup.removeClass("d-none");
        destinationSelector.removeClass("d-none");
        portSelector.addClass("d-none");
    });

    $("#result-container").on("click", ".pagination-control", function(){
        
        var searchForm = $(".search-form").serializeArray();
        var filterForm =  $(".filter-form").serializeArray();
        var pageValue = $(this).data("value");

        filterForm.push({'name': 'page', 'value': ''+pageValue});

        $.merge(searchForm, filterForm);

        var data = convertToObject(searchForm);

        console.log(searchForm);
        
        
        //var data = convertToObject(form);
        //data["page"] = pageValue;
        //alert(pageValue);

        $.request('SearchHandler::onFilter', {
         'data': data,
         'update': {
             'products/products-results': '#result-container'
         }
         }).then(function(){
             //console.log("aqui");
             window.scroll({
                 top: -2500, 
                 left: 0, 
                 behavior: 'smooth'
               });
         });
         //alert($(this).data("value"));*/

     });
});

function convertToObject(array){
    var obj = {};
    array.forEach(element => {
        obj[element.name]= element.value;
        
    });
    return obj;
}