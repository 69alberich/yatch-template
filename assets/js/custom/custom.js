function convertToObject(array) {
  var obj = {};
  array.forEach(element => {
    obj[element.name] = element.value;
  });
  return obj;
}