
function validateForm(form) {
  return new Promise((resolve, reject) => {

    let valid = true;
    $(form.children).toArray().forEach(group => {

      const input = $(group).children('input')[0];
      const errors = $(group).children('.errors')[0];

      const isRequired = $(group).attr('data-required');
      const pattern = $(group).attr('data-pattern');

      $(input).focus(function() {
        $(errors).empty();
      });

      if (isRequired && !input.value) {
        $(errors).empty();
        $(errors).append('<span class="error">Campo requerido</span>');
        valid = false;
      }

      if (pattern) {
        if (!(new RegExp(pattern)).test(input.value)) {
          $(errors).empty();
          $(errors).append('<span class="error">Introduzca un valor válido</span>');
          valid = false;
        }
      }
    });

    resolve(valid); 
    return;
  });
}

