

/*const openCartButton = document.getElementById('open-cart');
const closeCartButton = document.getElementById('close-cart');

openCartButton.addEventListener('click', function(e) {
  e.preventDefault();
  console.log('Here')
  cart.classList.add('open');
  $('html, body').css({
    overflow: 'hidden',
    height: '100%'
  });
});


closeCartButton.addEventListener('click', function(e) {
  e.preventDefault();
  cart.classList.remove('open');
  $('html, body').css({
    overflow: 'auto',
    height: 'auto'
  });
});*/

$(document).ready(function(){
  
  $("body").on("click", '#open-cart', function(e){
    e.preventDefault();
    $("body #cart").addClass("open");
    $('html, body').css({
      overflow: 'hidden',
      height: '100%'
    });
  
  });
  
  $("body").on("click", '#close-cart', function(e){
    e.preventDefault();
    $("body #cart").removeClass("open");
    $('html, body').css({
      overflow: 'auto',
      height: 'auto'
    });
    
  });
});

