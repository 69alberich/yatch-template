$("#country-codes").selectpicker();

$('#register-form').on('submit', async function (e) {
  e.preventDefault();
  var messages = "";
  const form = $(this).serializeArray();
  var obForm = convertToObject(form);
  console.log(obForm);
  messages = validateRegister(obForm);

  if(messages !=""){
    toastr.warning(messages, "Espera un segundo");
  }else{

    $(this).request('Registration::onAjax', {
      data:{
        phone: obForm.country_code+"-"+obForm.phone,
        property:{        
            document: obForm.document_code+"-"+obForm.document
        }
      },
      success: function (data) {
        console.log(data);
        if (typeof data.message !== 'undefined') {
          if (data.message.endsWith("is busy")) {
            toastr.warning("Tu email ya está registrado, verifícalo o intenta recuperar tu contraseña", "Espera un segundo");
          }else{
            toastr.warning(data.message, "Espera un segundo");
          }
        }
        this.success(data).then(function() {
          console.log('done');
        });
      }
    });
  }

  
/*
  const valid = await validateForm(this);
  if (!valid) return;
*/
  
});

$('#login-form').on('submit', async function (e) {
  e.preventDefault();
  const form = $(this).serializeArray();
  convertToObject(form);

  const valid = await validateForm(this);
  if (!valid) return;

  $(this).request('Login::onAjax', {
    success: function (data) {
      this.success(data).then(function() {
        console.log('done');
      });
    }
  });
});

$('#logout').on('click', function (e) {
  e.preventDefault();
  $(this).request('Logout::onAjax', {
    success: function (data) {
      this.success(data).then(function() {
        console.log('logout');
      });
    }
  });
});

$('#restore-password-form').on('submit', async function (e) {
  e.preventDefault();
  const form = $(this).serializeArray();
  convertToObject(form);

  const valid = await validateForm(this);
  if (!valid) return;

  $(this).request('RestorePassword::onAjax', {
    success: function (data) {
      console.log(data);
      this.success(data).then(function() {
        console.log('done');
      });
    }
  });
});

$('#reset-password-form').on('submit', async function (e) {
  e.preventDefault();
  const form = $(this).serializeArray();
  convertToObject(form);

  const valid = await validateForm(this);
  if (!valid) return;

  $(this).request('ResetPassword::onAjax', {
    success: function (data) {
      console.log(data);
      this.success(data).then(function() {
        console.log('done');
      });
    }
  });
});

$('#user-data-form').on('submit', async function (e) {
  e.preventDefault();
  const form = $(this).serializeArray();
  convertToObject(form);

  const valid = await validateForm(this);
  if (!valid) return;

  $(this).request('UserPage::onAjax', {
    success: function (data) {
      this.success(data).then(function() {
        console.log('done');
      });
    }
  });
});

$('#change-password-form').on('submit', async function (e) {
  e.preventDefault();
  const form = $(this).serializeArray();
  convertToObject(form);

  const valid = await validateForm(this);
  if (!valid) return;

  $(this).request('ChangePassword::onAjax', {
    success: function (data) {
      this.success(data).then(function() {
        console.log('ChangePassword');
      });
    }
  });
});

function validateRegister(data){
  var errorMessage ="";
      
       if(data["name"]===""){
          errorMessage+="*Tu nombre es obligatorio <br>";
       }
       if(data["last_name"]===""){
          errorMessage+="*Tu apellido es obligatorio <br>";
       }
       if(data["email"]===""){
          errorMessage+="*Tu email es obligatorio <br>";
       }
      
      if(data["phone"]==="" && data["phone"].length < 7){
          errorMessage+="*Tu telefono es obligatorio y debe tener al menos 8 caracteres <br>";
      }

      if(data["document"]==="" && data["document"].length < 7){
        errorMessage+="*Tu documento de identidad es obligatorio y debe tener al menos 7 caracteres <br>";
      }

      if(data["password"]==="" && data["password"].length < 7){
        errorMessage+="*Tu contraseña de identidad es obligatoria y debe tener al menos 8 caracteres <br>";
      }

      if(data["password"] != data["password_confirmation"]){
        errorMessage+="*Tu confirmación de contraseña no coincide <br>";
      }
       
  return errorMessage;
}