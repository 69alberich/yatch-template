$('#contact-form').on('submit', async function (e) {
    e.preventDefault();
    var messages = "";
    
    const form = $(this).serializeArray();
    var obForm = convertToObject(form);
    console.log(obForm);
    messages = validateForm(obForm);
  
    if(messages !=""){
      toastr.warning(messages, "Espera un segundo");
    }else{
  
      $(this).request('ContactFormHandler::onSentForm', {
        data:{
          phone: obForm.country_code+"-"+obForm.phone,
        },
        success: function (data) {
          console.log(data);
          if (typeof data.message !== 'undefined') {
            if (data.message.endsWith("is busy")) {
              toastr.success("Tu email ya está registrado, verifícalo o intenta recuperar tu contraseña", "Espera un segundo");
            }else{
              toastr.warning(data.message, "Espera un segundo");
            }
          }
          this.success(data).then(function() {
            toastr.success("Hemos recibido tu mensaje, nos comunicaremos contigo a la brevedad", "Gracias");
          });
        }
      });
    }
    
  });

  function validateForm(data){
    var errorMessage ="";
    if(data["name"]===""){
      errorMessage+="*Tu nombre es obligatorio <br>";
    }
    if(data["lastname"]===""){
        errorMessage+="*Tu apellido es obligatorio <br>";
    }
    if(data["email"]===""){
        errorMessage+="*Tu email es obligatorio <br>";
    }
    
    if(data["phone"]==="" && data["phone"].length < 7){
        errorMessage+="*Tu telefono es obligatorio y debe tener al menos 8 caracteres <br>";
    }
    return errorMessage;
  }