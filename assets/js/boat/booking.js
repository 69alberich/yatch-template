$("document").ready(function () {
    var flagOrder = true;
     loaderModal = null;
    var cartScrollBar = null;
    var flagPayment = true;

    var date = $("#created_at").data("date");
    
    if($("#clock").length > 0){

        $("#clock").countdown(date, function(event) {
            $(this).html(event.strftime('%Hh %Mm %Ss'));
        });
    }
    
    $("#order-form").on("submit", function(e){
        e.preventDefault();
        
        
        $(this).request('CartSessionList::onCreateOrder',{
            success: function(data){
                //console.log(data);
                if(data["error"]){
                    toastr.error(data["error"][0], "Espera", 
                    {timeOut: 1500, positionClass: 'toast-bottom-right'});
                }else{
                    //$(location).attr('href', 'http://localhost/dictya/order/'+data.secret);
                    $(location).attr('href', 'https://rentatuyate.com/orden/'+data.secret_key);
                }
                
            }
        });
    });

    $("#country-codes").selectpicker();

    $(".boat-booking-form").on("submit", function (e) {
        e.preventDefault();

        $(this).request('BoatBookingHandler::onFindBoatPrice',{
            'update': {
                'booking/boat-booking-price': '.result-price-container'
            }
        });
    });
    
    $(".boat-booking-form").on("change", "#booking-type", function (e) {
        e.preventDefault();

        $(this).request('BoatBookingHandler::onChangeBookingType',{
            'update': {
                'booking/calendar': '.calendar-content',
                'booking/boat-booking-price': '.result-price-container'
            }
        });
    });

    $(".result-price-container").on("click", "#add-to-cart", function(e){
        e.preventDefault();
        
        $(this).request('CartSessionList::onAddCharge',{
            data: {
                'product_type': 'Lovata\Shopaholic\Models\Product'
            },
            'update': {
                'booking/calendar': '.calendar-content',
                'booking/boat-booking-price': '.result-price-container',
                'cart': '#cart-container',
                'header-top': '#header-top-container'
            }
        }).then(()=>{
            $("body #cart").addClass("open");
            $('body .boat-booking-form').trigger("reset");
        });
        
      });

    
      $("#cart-container").on("click", ".remove-from-cart", function(e){
        $.request("CartSessionList::onRemoveCharge", {
            data:{
                index: $(this).data("index")
            },
            'update': {
                'booking/calendar': '.calendar-content',
                'booking/boat-booking-price': '.result-price-container',
                'cart': '#cart-container',
                'header-top': '#header-top-container'
            }
        }).then(()=>{
            $("body #cart").addClass("open");
        });
        //alert("tengo:"+$(this).data("index"));
    });

    $('#payment-form').on('submit', function(e){
       
        var request = $(this).payStripe();
        
        request.then(function(response){
        var respuesta = JSON.parse(response.result);
        //console.log("respuesta", respuesta);
        
        
        if(respuesta.status === "succeeded"){
            //abre aqui
            var alerta_stripe = swal({
            title: 'Hemos procesado su pago correctamente, ',
            text: 'Espere mientras registramos su compra',
            closeOnEsc:false,
            allowEnterKey: false,
            allowOutsideClick: false,
            button:true,
            }).then( response => {
            var data = {
                reference: respuesta["reference"],
                method_id: 1,
                currency: 1,
                status_id: 1,
                order_status_id: 5,
                
            }
          
                $.request('PaymentsHandler::onAddPayment', {
                    data: data,
                    success: function(response) {
                       
                        swal.close();
                        swal({
                        title: '¡Proceso Completado!',
                        content: 'Gracias por Reservar con nosotros',
                        
                        }).then(function(){
                            location.reload();
                        }

                        );
                    },
                    error: function(response){
                    swal.close();
                    swal({
                    
                        title: 'Ha ocurrido un error',
                        content: 'Intenta nuevamente por favor',
                        onClose: function(){
                        location.reload();
                        }
                    });
                    }
                });
  
            },
            function(){
            //
            });
            //termina aqui
            
        }else{
            //console.log(response);
            swal({
            title: 'Ha ocurrido un error',
            content: "Intenta nuevamente por favor",
            onClose: function(){
                location.reload();
            }
            });
            } 
        },

        
            function(response){
            swal({
                title: 'Disculpe, Ha fallado la pasarela de pagos',
            }).then(function(){
                location.reload()
                });
        });
        e.preventDefault();
    });
    
    $(".file-payment-form").on("submit", function(e){
        e.preventDefault();

        var form = $(this).serializeArray();
        var data = convertToObject(form);

        var message = "";
    
        if(typeof data["reference"] == "undefined" || data["reference"] =="" || data["reference"].lenght <4){

            message+="*Debes escribir un numero de referencia";

        }

        if (message!="") {
            alert($message);
            
        }else{
            if(flagPayment){
                flagPayment = false;
                $(this).request('PaymentsHandler::onAddPayment', {
                    'success': function(response){
                        console.log(response);
                       
                        swal({
                            title: "¡Bien hecho!",
                            text: "Hemos recibido tu comprobante",
                            icon: "success",
                            button: "aceptar",
                          }).then((value)=>{
                              location.reload();
                          });
                          
                    },
                    'error': function(response){
                        console.log(response);
                        swal({
                            title: "¡Oops!",
                            text: "Ha ocurrido un error, intenta nuevamente",
                            icon: "error",
                            button: "aceptar",
                          }).then((value)=>{
                              location.reload();
                          });
                    },
                    'complete':function(response){
                        flagPayment = true;
                    }
                });
            }else{
                console.log("subiendo");
            }
            
        }
  
    });

    
});